import argparse
import httplib
import httplib2
import os
import random
import time
import moviepy
import datetime
import urllib
import re

from moviepy.editor import VideoFileClip, concatenate_videoclips
from bs4 import BeautifulSoup, SoupStrainer
from selenium import webdriver
from urllib import urlopen

import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow


# Explicitly tell the underlying HTTP transport library not to retry, since
# we are handling retry logic ourselves.
httplib2.RETRIES = 1

# Maximum number of times to retry before giving up.
MAX_RETRIES = 10

# Always retry when these exceptions are raised.
RETRIABLE_EXCEPTIONS = (httplib2.HttpLib2Error, IOError, httplib.NotConnected,
  httplib.IncompleteRead, httplib.ImproperConnectionState,
  httplib.CannotSendRequest, httplib.CannotSendHeader,
  httplib.ResponseNotReady, httplib.BadStatusLine)

# Always retry when an apiclient.errors.HttpError with one of these status
# codes is raised.
RETRIABLE_STATUS_CODES = [500, 502, 503, 504]

# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret. You can acquire an OAuth 2.0 client ID and client secret from
# the {{ Google Cloud Console }} at
# {{ https://cloud.google.com/console }}.
# Please ensure that you have enabled the YouTube Data API for your project.
# For more information about using OAuth2 to access the YouTube Data API, see:
#   https://developers.google.com/youtube/v3/guides/authentication
# For more information about the client_secrets.json file format, see:
#   https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
CLIENT_SECRETS_FILE = r'./client_secret_1050996704497-k1dn1rcjs271t2a77c1p35lqnrq9uqek.apps.googleusercontent.com.json'

# This OAuth 2.0 access scope allows an application to upload files to the
# authenticated user's YouTube channel, but doesn't allow other types of access.
SCOPES = ['https://www.googleapis.com/auth/youtube.upload']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

VALID_PRIVACY_STATUSES = ('public', 'private', 'unlisted')


# Authorize the request and store authorization credentials.
def get_authenticated_service():
  flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
  credentials = flow.run_console()
  return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)

def initialize_upload(youtube, options):
  tags = None
  if options.keywords:
    tags = options.keywords.split(',')

  body=dict(
    snippet=dict(
      title=options.title,
      description=options.description,
      tags=tags,
      categoryId=options.category
    ),
    status=dict(
      privacyStatus=options.privacyStatus
    )
  )

  # Call the API's videos.insert method to create and upload the video.
  insert_request = youtube.videos().insert(
    part=','.join(body.keys()),
    body=body,
    # The chunksize parameter specifies the size of each chunk of data, in
    # bytes, that will be uploaded at a time. Set a higher value for
    # reliable connections as fewer chunks lead to faster uploads. Set a lower
    # value for better recovery on less reliable connections.
    #
    # Setting 'chunksize' equal to -1 in the code below means that the entire
    # file will be uploaded in a single HTTP request. (If the upload fails,
    # it will still be retried where it left off.) This is usually a best
    # practice, but if you're using Python older than 2.6 or if you're
    # running on App Engine, you should set the chunksize to something like
    # 1024 * 1024 (1 megabyte).
    media_body=MediaFileUpload(options.file, chunksize=-1, resumable=True)
  )

  resumable_upload(insert_request)

# This method implements an exponential backoff strategy to resume a
# failed upload.
def resumable_upload(request):
  response = None
  error = None
  retry = 0
  while response is None:
    try:
      print 'Uploading file...'
      status, response = request.next_chunk()
      if response is not None:
        if 'id' in response:
          print 'Video id "%s" was successfully uploaded.' % response['id']
        else:
          exit('The upload failed with an unexpected response: %s' % response)
    except HttpError, e:
      if e.resp.status in RETRIABLE_STATUS_CODES:
        error = 'A retriable HTTP error %d occurred:\n%s' % (e.resp.status,
                                                             e.content)
      else:
        raise
    except RETRIABLE_EXCEPTIONS, e:
      error = 'A retriable error occurred: %s' % e

    if error is not None:
      print error
      retry += 1
      if retry > MAX_RETRIES:
        exit('No longer attempting to retry.')

      max_sleep = 2 ** retry
      sleep_seconds = random.random() * max_sleep
      print 'Sleeping %f seconds and then retrying...' % sleep_seconds
      time.sleep(sleep_seconds)
def remove_illegal_chars(string):
	return re.sub(r'[\\/:\*?|<>\'"!]', '', string)

def video_links_with_titles():
	d = dict()
	browser = webdriver.PhantomJS()
	response = urlopen('https://oddshot.tv/?order=mostViewed&period=day')
	soup = BeautifulSoup(response, 'lxml')
	shotlist_wrapper = soup.body.find(id = 'react-root').div.div.next_sibling.div.next_sibling.next_sibling.div.div.next_sibling.div.div.next_sibling.div.next_sibling.div.div
	for item in shotlist_wrapper:
		title = item.div.next_sibling.div.a['title']
		title = remove_illegal_chars(title)
		video_page = item.div.next_sibling.div.a['href']
		link = 'https://oddshot.tv' + video_page
		browser.get(link)
		response_video = browser.page_source
		soup_video = BeautifulSoup(response_video, 'lxml')
		script_txt = soup_video.find('script', {'id':'preloaded-data'}).text.encode('utf-8')
		video_url = re.search(r'https://oddshot.akamaized.net/m/render-captures/source/[^"]+mp4', script_txt).group()
		d.update({title : video_url})
	print str(len(d)) + ' videos are downloading.'
	print d
	return d
def download_and_concetanate(dictionaryOfLinks):
	video_list = list()
	for title, link in dictionaryOfLinks.items():
		video = urlopen(link)
		print(link)
		with open(title+'.mp4', 'wb') as v_file:
			v_file.write(video.read(), preset = 'slower')
			v_file.close()
	for title in dictionaryOfLinks:
		try:
			video_list.append(VideoFileClip(title + '.mp4'))
		except:
			print 'VideoFileError :' + title + ': ' + link
	today = datetime.datetime.now().utctimetuple()
	final_clip = concatenate_videoclips(video_list)
	videoname = str(today[0])+ str(today[1]) + str(today[2])
	final_clip.write_videofile(videoname + ".mp4")
	log_file()#Move this to upload session
def is_today_done():
	today = datetime.datetime.now().utctimetuple()
	with open('log.txt', 'r') as log:
		r = log.readlines()[::-1]
		if r[0] == today:
			return True
		else:
			return False
	return False
def log_file():
	date = datetime.datetime.now().utctimetuple()
	with open('log.txt', 'a') as log:
		today = str(date[0])+ str(date[1]) + str(date[2]) + '\n'
		log.write(today)
		log.close()
	
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--file', help='Video file to upload')
  parser.add_argument('--title', help='Video title', default='Test Title')
  parser.add_argument('--description', help='Video description',
    default='Test Description')
  parser.add_argument('--category', default='22',
    help='Numeric video category. ' +
      'See https://developers.google.com/youtube/v3/docs/videoCategories/list')
  parser.add_argument('--keywords', help='Video keywords, comma separated',
    default='test,check')
  parser.add_argument('--privacyStatus', choices=VALID_PRIVACY_STATUSES,
    default='private', help='Video privacy status.')
  args = parser.parse_args()
  os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
  youtube = get_authenticated_service()

  try:
	while(True):
		if is_today_done() == False:
			links = video_links_with_titles()
			download_and_concetanate(links)
			time.sleep(50000)
		time.sleep(7200)
		initialize_upload(youtube, args)
  except HttpError, e:
    print 'An HTTP error %d occurred:\n%s' % (e.resp.status, e.content)